CONTENTS OF THIS FILE
---------------------

* Introduction
* Requirements
* Recommended Modules
* Installation
* Configuration
* Additional Licenses
* Maintainers

INTRODUCTION
------------

The ***Laces Base*** module installs features and support code for creating a basic website using the Layout Builder
ecosystem and the [***Laces*** ecosystem][Laces Ecosystem]
that uses [***Bootstrap 5***][Bootstrap 5]. 

Layouts provided by layout discovery and layout builder are removed. A new content type is created and a variety of
media settings are created.

[***Bootstrap Styles***][Bootstrap Styles] module settings are changed to support custom 
[***Bootstrap 5***][Bootstrap 5] styles configured by the [***Laces***][Laces] theme.

* For a full description of the module, visit the project page:
  https://www.drupal.org/project/laces_base

* To submit bug reports and feature suggestions, or track changes:
  https://www.drupal.org/project/issues/laces_base

> ### [**Laces** ecosystem][Laces Ecosystem]
> Create a functional starter website by installing the [**Laces** ecosystem][Laces Ecosystem].
>
> [Laces][Laces] is a [***Bootstrap 5***][Bootstrap 5] theme with enhancements for the [***Layout Builder*** ecosystem][Layout Builder Ecosystem].
>
> [Laces Base][Laces Base] is an installation and support module for creating a base website that uses the ***Layout Builder*** ecosystem.
>
> [Laces Blocks] (In development) that provides blocks for Bootstrap 5 components.

REQUIREMENTS
------------

This theme requires the following modules:

- drupal:node
- drupal:breakpoint
- drupal:layout_builder
- drupal:layout_discovery
- drupal:media
- drupal:media_library
- drupal:responsive_image
- [Bootstrap Layout Builder][Bootstrap Layout Builder]
- [Bootstrap Styles][Bootstrap Styles]
- [Layout Builder Blocks][Layout Builder Blocks]
- [Crop][Crop]
- [Image Effects][Image Effects]
- [Focal Point][Focal Point]

RECOMMENDED MODULES
-------------------

* [***Laces*** (theme)][Laces]
* [***Laces Blocks***][Laces Blocks] (coming soon)

INSTALLATION
------------

Install as you would normally install a contributed Drupal module. Visit
[Installing Modules][Installing Modules] for further information.

CONFIGURATION
-------------

***Laces Base*** provides theming for edit pages and the 'Content' administration pages. On the Appearance page
uncheck 'Use the administration theme when editing or creating content.'

MAINTAINERS
-----------

* [John Rasmussen (bigbaldy)][bigbaldy]

[bigbaldy]: https://www.drupal.org/u/bigbaldy
[Bootstrap 5]: https://getbootstrap.org
[Bootstrap Layout Builder]: https://www.drupal.org/project/bootstrap_layout_builder
[Bootstrap Styles]: https://www.drupal.org/project/bootstrap_styles
[Crop]: https://www.drupal.org/project/crop
[Focal Point]: https://drupal.org/project/focal_point
[Image Effects]: https://drupal.org/project/image_effects
[Installing Modules]: https://www.drupal.org/node/895232/
[Laces]: https://www.drupal.org/project/laces
[Laces Base]: https://www.drupal.org/project/laces_base
[Laces Blocks]: https://www.drupal.org/project/laces_blocks
[Layout Builder Blocks]: https://www.drupal.org/project/layout_builder_blocks
[Laces Ecosystem]: https://www.drupal.org/project/laces/ecosystem
[Layout Builder Ecosystem]: https://www.drupal.org/project/layout_builder/ecosystem
