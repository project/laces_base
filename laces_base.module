<?php

/**
 * @file
 * Primary module hooks for Laces Base module.
 */

use Drupal\Core\Routing\RouteMatchInterface;

/**
 * Implements hook_help().
 */
function laces_base_help($route_name, RouteMatchInterface $arg) {
  switch ($route_name) {
    case 'help.page.laces_base':
      $output = '';
      $output .= '<h3>' . t('About') . '</h3>';
      $output .= '<p>' . t('The <em>Laces Base</em> module installs features and support code for creating a basic website using the Layout Builder ecosystem.');
      $output .= '<p>' . t('For more information, see the module page at <a href=":LB-project">Laces Base</a>.', [':LB-project' => 'https://www.drupal.org/project/laces_base']) . '</p>';
      return $output;
  }
}

/**
 * Implements hook_plugin_filter_TYPE__CONSUMER_alter().
 */
function laces_base_plugin_filter_layout__layout_builder_alter(array &$definitions, array $extra) {
  // Remove layouts provide by layout discovery and layout builder.
  $duplicate_layout_discovery = [
    'layout_onecol',
  ];
  $duplicate_layout_builder = [
    'layout_onecol',
    'layout_twocol_section',
    'layout_threecol_section',
    'layout_fourcol_section',
  ];
  foreach ($duplicate_layout_discovery as $duplicate_layout) {
    /** @var \Drupal\Core\Layout\LayoutDefinition[] $definitions */
    if (isset($definitions[$duplicate_layout])) {
      if ($definitions[$duplicate_layout]->getProvider() === 'layout_discovery') {
        unset($definitions[$duplicate_layout]);
      }
    }
  }
  foreach ($duplicate_layout_builder as $duplicate_layout) {
    /** @var \Drupal\Core\Layout\LayoutDefinition[] $definitions */
    if (isset($definitions[$duplicate_layout])) {
      if ($definitions[$duplicate_layout]->getProvider() === 'layout_builder') {
        unset($definitions[$duplicate_layout]);
      }
    }
  }
}
